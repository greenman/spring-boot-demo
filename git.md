#Git
1. 在一个目录下执行git init，会将当前目录创建为git仓库
`git init`
2. 执行git add . 把当前目录下所有文件添加到仓库
`git add . `
3. 把添加的文件提交到本地仓库
`git commit -m 'First commit'  `
4. 添加remote及验证remote。
`git remote add origin *git地址* `
5.  把本地的repository push到github。其间需要输入github的账号和密码
`git push origin master`