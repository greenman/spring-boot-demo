package com.coderme.bd;

import org.springframework.boot.ResourceBanner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 该类必须在要扫描的包的上一级目录
 * 一个@SpringBootApplication相当
 * 于@Configuration,@EnableAutoConfiguration和 @ComponentScan 并具有他们的默认属性值
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan
public class Application {
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(Application.class);
        app.run(Application.class);
    }
}