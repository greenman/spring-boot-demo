package com.coderme.bd.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.CounterService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingsController {
    @Autowired
    private CounterService counterService;

    @RequestMapping("/greet")
    public String greet() {
        counterService.increment("myapp.greet.count");
        return "Hello!";
    }
}