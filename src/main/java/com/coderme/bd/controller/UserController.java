package com.coderme.bd.controller;

import com.coderme.bd.dao.UserDAO;
import com.coderme.bd.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

/**
 * Project:health-backend
 * Created By zhang tengfei
 * Date:2016/12/7
 * Time:15:16
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserDAO userDAO;

    @RequestMapping("/hello")
    public String hello(Model model) {
        model.addAttribute("hello", "from /user/hello");


        return "hello";
    }
    @RequestMapping("/home")
    public String home(Model model) {
        model.addAttribute("hello", "from /user/home");
        return "user/home";
    }

    @RequestMapping("/get-by-email")
    @ResponseBody
    public String getByEmail(@NotNull String email) {
        String userId;
        User user = userDAO.findByEmail(email);
        if (user != null) {
            userId = String.valueOf(user.getId());
            return "The user id is: " + userId;
        }
        return "user " + email + " is not exist.";
    }
    @RequestMapping("/get-by-name")
    @ResponseBody
    public String getByName(@NotNull String name) {
        String userId;
        User user = userDAO.findByName(name);
        if (user != null) {
            userId = String.valueOf(user.getId());
            return "The user id is: " + userId;
        }
        return "user " + name + " is not exist.";
    }
}
