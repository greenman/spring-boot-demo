package com.coderme.bd.dao;

import com.coderme.bd.entity.User;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * Spring-data-jpa通过解析方法名创建查询
 * <a href="http://www.jianshu.com/p/38d27b633d9c">参考文章</a>
 * Project:health-backend
 * Created By zhang tengfei
 * Date:2016/12/7
 * Time:17:03
 */
@Repository
public interface UserDAO extends PagingAndSortingRepository<User, Long>, JpaSpecificationExecutor<User> {
    public User findByEmail(String emails);

    @Query("from User t where t.name=:name")
    public User findByName(@Param("name") String name);

    public User findByNameAndEmail(String name, String emails);

}
