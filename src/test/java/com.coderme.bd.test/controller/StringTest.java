package com.coderme.bd.test.controller;

import java.util.Scanner;

public class StringTest {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = "";
        str = sc.nextLine();
        char[] array = str.toCharArray();

        int temp = 0;
        int length=array.length;
        for (int i = 0; i < length; i++) {
            temp = (int) array[i];
            if (temp <= 90 && temp >= 65) {
                array[i] = (char) (temp + 32);
            } else if (temp <= 122 && temp >= 97) {
                array[i] = (char) (temp - 32);
            }
        }

        System.out.println(String.valueOf(array));
    }
}
