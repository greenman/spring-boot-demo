package com.coderme.bd.test.dao;

import com.coderme.bd.Application;
import com.coderme.bd.dao.UserDAO;
import com.coderme.bd.entity.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.criteria.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes=Application.class)// 指定spring-boot的启动类
//@SpringApplicationConfiguration(classes = Application.class)// 1.4.0 前版本
public class ApplicationTests {

    @Autowired
    private UserDAO userDAO;

    @Test
    public void test() throws Exception {

        // 创建10条记录
        userDAO.save(new User("AAA", "AAA@coderme.cn"));
        userDAO.save(new User("BBB", "BBB@coderme.cn"));
        userDAO.save(new User("CCC", "CCC@coderme.cn"));
        userDAO.save(new User("DDD", "DDD@coderme.cn"));
        userDAO.save(new User("EEE", "EEE@coderme.cn"));
        userDAO.save(new User("FFF", "FFF@coderme.cn"));
        userDAO.save(new User("GGG", "GGG@coderme.cn"));
        userDAO.save(new User("HHH", "HHH@coderme.cn"));
        userDAO.save(new User("III", "III@coderme.cn"));
        userDAO.save(new User("JJJ", "JJJ@coderme.cn"));

        // 测试findAll, 查询所有记录
//        Assert.assertEquals(10, userDAO.findAll(new Specification<User>() {
//            @Override
//            public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
//                Path<String> namePath = root.get("name");
//                Path<String> nicknamePath = root.get("email");
//                /**
//                 * 连接查询条件, 不定参数，可以连接0..N个查询条件
//                 */
//                query.where(cb.like(namePath, "%李%"), cb.like(nicknamePath, "%王%")); //这里可以设置任意条查询条件
//                return null;
//            }
//        }).size());

//        // 测试findByName, 查询姓名为FFF的User
//        Assert.assertEquals("FFF", userDAO.findByName("FFF").getName());
//
//        // 测试findUser, 查询姓名为FFF的User
//        Assert.assertEquals(60, userDAO.findByName("FFF").getEmail());
//
//        // 测试findByNameAndAge, 查询姓名为FFF并且年龄为60的User
//        Assert.assertEquals("FFF", userDAO.findByNameAndEmail("FFF", "FFF@coderme.cn").getName());
//
//        // 测试删除姓名为AAA的User
//        userDAO.delete(userDAO.findByName("AAA"));

        // 测试findAll, 查询所有记录, 验证上面的删除是否成功
//        Assert.assertEquals(9, userDAO.findAll().size());
    }


}